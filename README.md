# Setting Your Data in Context – with PIDs

[Poster](FDT22Poster.pdf), [data](data) and [code](script.ipynb) for Aktionstag Forschungsdaten at Freie Universität, Okt 27th 2022.

Start a demonstrator via mybinder.org 👉 [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/webvstdlch%2FenrichmentPoster/HEAD?labpath=script.ipynb).


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
